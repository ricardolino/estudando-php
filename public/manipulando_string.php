<?php

$name = 'EspecializaTi - Cursos Online de TI';

echo strtoupper($name);
echo '<hr>';

echo strtolower($name);
echo '<hr>';

echo ucfirst(strtolower($name));
echo '<hr>';

echo ucwords(strtolower($name));
echo '<hr>';

$info = 'São Paulo/SP/Brasil/Terra';
$arrayInfo = explode('/', $info);
var_dump($arrayInfo[2]);
echo '<hr>';

$arrayTest = [1,2,3,4,5];
//echo implode($arrayInfo, ' # ');
echo implode($arrayTest, ' - ');
echo '<hr>';

$name = ' EspecializaTi ';
var_dump($name);
var_dump(ltrim($name));
var_dump(rtrim($name));
var_dump(trim($name));
echo '<hr>';

$domain = 'www.especializati.com.br';

echo str_replace('www.', 'https://www.', $domain);
echo '<hr>';

echo substr($domain, 0, 4);
echo '<br>';
echo substr($domain, -7);
echo '<hr>';

echo strlen($domain);
echo '<hr>';

$name = 'Carlos Ferreira';

if (isset($name)) //Retorna um boleano no caso da variável existir ou não
    echo $name;
else
    echo 'Não existe!';

unset($name); //Remove a variávle da memória
echo '<br>';

if (isset($name))
    echo $name;
else
    echo 'Não existe!';
echo '<hr>';

date_default_timezone_set('America/Sao_Paulo');
echo 'O ano atual: ' . date('Y') . '<br>';
echo 'O mês atual: ' . date('m') . '<br>';
echo 'O dia atual: ' . date('d') . '<br>';
echo 'A data atual: ' . date('d/m/Y') . '<br>'; // O seperador pode ser qualquer símbolo
echo 'A data atual: ' . date('Y-m-d') . '<br>';
echo 'Hora atual: ' . date('H') . '<br>'; // H - 24 horas e h 12 em 12 horas
echo 'Minuto atual: ' . date('i') . '<br>';
echo 'Segundo atual: ' . date('s') . '<br>';
echo 'Hora atual: ' . date('H:i:s') . '<br>';
echo 'Timezone: ' . date_default_timezone_get() . '<br>';
echo '<hr>';

$password = '123456';

echo md5($password);
echo '<br>';
echo sha1($password);
echo '<br>';
echo crypt($password, $salt);
echo '<br>';
$passCr = base64_encode($password);
echo $passCr;
echo '<br>';
echo base64_decode($passCr);
echo '<br>';
echo hash('sha512', $password);
