<?php
$name = 'Carlos';
$company = 'EspecializaTi';
$year = 2018;

// $infoCompany = [
//     $name,
//     $company,
//     $year
// ];

// compact - gera um array com as váriavies, no momento de declarar as variáveis não é necessário o $
$infoCompany = compact('name', 'company', 'year');
var_dump($infoCompany);
echo '<br>';

//is_array - Retorna booleano em caso se ser ou não array
var_dump(is_array($infoCompany));
echo '<br>';

//in_array - Retorna booleano em caso de ter ou não elemento dentro da array
var_dump(in_array('Carlos', $infoCompany));
echo '<br>';

$infoCompany = [
    'name'          => 'EspecializaTi',
    'founder'       => 'Carlos Ferreira',
    'year_current'  => 2018,
];

$infoCompanyTwo = [
    'courses'       => ['PHP', 'JS', 'Vue JS', 'Laravel'],
    'total_courses' => 26,
];

//array_keys - Retorna as chaves da array
var_dump(array_keys($infoCompany));
echo '<br>';

//array_values - Retorna os valores da array
var_dump(array_values($infoCompany));
echo '<br>';

//count - Retorna a quantidade de valores
var_dump(count($infoCompany));
echo '<br>';

//array_merge - faz o merge entre duas arrays
$infoCompany = array_merge($infoCompany, $infoCompanyTwo);
echo '<pre>';
var_dump($infoCompany);
echo '<br>';

$cart = ['Arroz', 'Sabão', 'Feijão', 'Balinhas'];
var_dump($cart);

echo '<hr>';

//array_pop - remove o ultimo elemento da lista
array_pop($cart);
var_dump($cart);

echo '<hr>';

//array_shift - remove o primeiro elemento da lista
array_shift($cart);
var_dump($cart);

echo '<hr>';

//unset - remove o elemento da lista, caso seja passado sem i=um index vai apagar toda lista
unset($cart[0]);
var_dump($cart);

echo '<hr>';

//array_push - adiciona um elemento no final da lista
array_push($cart, 'Tapete');
array_push($cart, 'Rodo');
var_dump($cart);

echo '<hr>';

//array_unshift - adiciona um elemento no início da lista
array_unshift($cart, 'Microondas');
array_unshift($cart, 'Tapete');
var_dump($cart);

echo '<hr>';

//array_unique - devolve a mesma array com os valores únicos, caso tenha valores repetidos
$cart = array_unique($cart);
var_dump($cart);

$cart = [
    0 => 'Macarrão',
    1 => 'Feijão',
    2 => 'Arroz',
    3 => 'Batata'
];

//arsort - arruma uma lista em ordem alfabetica, caso tenha chaves vai arrumar as chaves
echo '<pre>';
arsort($cart);
var_dump($cart);

echo '<hr>';

//asort - arruma a lista em ordem alfabética intependente das chaves
asort($cart);
var_dump($cart);

echo '<hr>';

//sort - arruma a lista a lista em ordem alfabética e mantem as chaves na ordem
sort($cart);
var_dump($cart);

echo '<hr>';

$ages = [12, 14, 18, 20, 44, 50, 98, 78, 56];

// echo $ages[count($ages) - 1];
//end - retorna o ultimo elemento da lista
echo end($ages);

//array_filter - lambda que permite criar filtros e devolver lista de valores que passarem
$agesFiltered = array_filter($ages, function ($age) {
    return $age >= 18;
});

var_dump($agesFiltered);

$names = ['Carlos', 'EspecializaTi', 'Company'];
/*
$names[0] = strtoupper($names[0]);
$names[1] = strtoupper($names[1]);
$names[2] = strtoupper($names[2]);
*/
//array_map - lambda que permite alterar valores dentro da lista
$names = array_map('applyToupper', $names);

function applyToupper($value)
{
    return strtoupper($value);
}

var_dump($names);