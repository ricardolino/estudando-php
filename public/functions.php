<?php

function testFunction(): String
{
    return 'Sou uma função <br>';
}

echo testFunction();

echo '<hr>';

function pt(String $value)
{
    echo "{$value} <br>";
}
pt('Algo');

echo '<hr>';

function sum(int $n1, int $n2): int
{
    return $n1 + $n2;
}
echo sum(12, 90);
echo '<hr>';

function sumTwo(int $n1, int $n2, int $taxa = 2):int
{
    return ($n1 + $n2) * $taxa;
}
echo sumTwo(1,2);
echo '<hr>';
echo sumTwo(1,2, 3);
echo '<hr>';


$taxa = 2;

function sum2(int $n1, int $n2):Array
{
    global $taxa;

    $teste = 12;
    $soma = ($n1 + $n2) * $taxa;

    return [
        'teste' => $teste,
        'soma'  => $soma,
    ];
}

//$result = sum(1,2);
//var_dump($result['soma']);
//echo sum(2,2);
echo sum2(2,2)['soma'];

echo '<hr>';

// 4 => 4*3*2*1 = 24
// 2 => 2*1 = 2
// 3 => 3*2*1 = 6

function factorial(int $number): int
{
    if ($number <= 1)
        $number = $number;
    else
        $number *= factorial($number -1);

    return $number;
}

echo factorial(4);