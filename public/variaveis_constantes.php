
<?php

//Variáveis
$name = 'Carlos Ferreira';

echo $name;
echo '<br>';

$name = 'Carlos Ferreira Teste';
echo $name;
echo '<br>';

$name = 12.2;
echo $name;
echo '<br>';

$name = 123;
echo $name;
echo '<br>';

//Constantes
define('NOME_CONSTANTE', 123);

var_dump(NOME_CONSTANTE);
echo '<br>';

const TESTE_CONSTANTE = 321;

var_dump(TESTE_CONSTANTE);