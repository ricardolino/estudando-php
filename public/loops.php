<?php

$names = ['a', 'b', 'c', 'd', 'e', 'f'];

echo '<hr>';

for ($i = 0; $i<count($names); $i++) {
    echo $names[$i];
}

echo '<hr>';

$names = [
    'a' => 'aa',
    'b' => 'bb',
    'c' => 'cc',
    'd' => 'dd',
    'e' => 'ee',
    'f' => 'ff',
    'g' => 'gg',
    'h' => 'hh',
];

foreach ($names as $key => $name) {
    echo "{$name} <br>";
}

echo '<hr>';

$names = ['a', 'b', 'c', 'd'];

$i = 0;

while ($i < count($names)) {
    echo $names[$i];

    $i++;
}

echo '<hr>';

$a = 11;
do {
    echo $a;

    $a++;
} while ($a <= 10);